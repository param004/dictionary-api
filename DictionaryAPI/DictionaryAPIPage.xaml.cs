﻿using System;
using System.Collections.Generic;
using System.Linq;
using Plugin.Connectivity;
using System.Net.Http;
using DictionaryAPI.Models;
using Xamarin.Forms;

namespace DictionaryAPI
{
    public partial class DictionaryAPIPage : ContentPage
    {
        public DictionaryAPIPage()
        {
            InitializeComponent();
        }


        void Handle_ClickedWifi(object sender, System.EventArgs e)
        {
            if (CrossConnectivity.Current.IsConnected == true)
            {
                ConnectionStatusLabel.Text = "You are connected via " +
                    CrossConnectivity.Current.ConnectionTypes.First();
            }
            else
            {
                ConnectionStatusLabel.Text = "You are not connected to Internet";
            }
        }



        async void Handle_Completed(object sender, System.EventArgs e)
        {
            if (CrossConnectivity.Current.IsConnected != true)
            {
                ConnectionStatusLabel.Text = "No Service, because you are not connected to the internet";
            }
            else
            {
                string searchWord = ((Entry)sender).Text;
                searchWord = searchWord.ToLower();
                //Type.Text = searchWord;
                HttpClient client = new HttpClient();

                var uri = new Uri(

                    string.Format($"https://owlbot.info/api/v2/dictionary/" + $"{searchWord}"));


                var request = new HttpRequestMessage();
                request.Method = HttpMethod.Get;
                request.RequestUri = uri;
                request.Headers.Add("Application", "application / json");


                List<DictionaryItem> DictionaryApiData = null;
                HttpResponseMessage response = await client.SendAsync(request);

                if (response.IsSuccessStatusCode)
                {
                    var content = await response.Content.ReadAsStringAsync();
                    if (content == "[]")
                    {
                        Type.Text = "Invalid Word";
                        Definition.Text = "Word Does Not Exist";
                        Example.Text = "Please Try Again";
                    }
                    else
                    {
                        DictionaryApiData = DictionaryItem.FromJson(content);

                        Type.Text = "Type: " + DictionaryApiData[0].Type;
                        Definition.Text = "Definition: " + DictionaryApiData[0].Definition;
                        Example.Text = "Example: " + DictionaryApiData[0].Example;

                    }
                }




            }

        }
    }
}
