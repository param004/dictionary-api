﻿// To parse this JSON data, add NuGet 'Newtonsoft.Json' then do:
//
//    using DictionaryItem;
//
//    var welcome = DictionaryItem.FromJson(jsonString);

namespace DictionaryAPI.Models
{
    using System;
    using System.Collections.Generic;
    using System.Net;

    using System.Globalization;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Converters;

    public partial class DictionaryItem
    {
        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("definition")]
        public string Definition { get; set; }

        [JsonProperty("example")]
        public string Example { get; set; }
    }

    public partial class DictionaryItem
    {
        public static List<DictionaryItem> FromJson(string json) => JsonConvert.DeserializeObject<List<DictionaryItem>>(json, DictionaryAPI.Models.Converter.Settings);
    }

    public static class Serialize
    {
        public static string ToJson(this List<DictionaryItem> self) => JsonConvert.SerializeObject(self, DictionaryAPI.Models.Converter.Settings);
    }

    internal static class Converter
    {
        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
            DateParseHandling = DateParseHandling.None,
            Converters = {
                new IsoDateTimeConverter { DateTimeStyles = DateTimeStyles.AssumeUniversal }
            },
        };
    }
}
